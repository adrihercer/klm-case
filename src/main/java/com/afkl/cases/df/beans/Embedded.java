package com.afkl.cases.df.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Embedded {

	private List<ElementResponse> locations;

	public List<ElementResponse> getLocations() {
		return locations;
	}

	public void setLocations(List<ElementResponse> locations) {
		this.locations = locations;
	}
}

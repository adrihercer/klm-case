package com.afkl.cases.df.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AirportsList {
	
	@JsonProperty("_embedded")
	private Embedded embedded;
	private Page page;
	
	public Embedded getEmbedded() {
		return embedded;
	}
	
	public void setEmbedded(Embedded embedded) {
		this.embedded = embedded;
	}
	
	public Page getPage() {
		return page;
	}
	
	public void setPage(Page page) {
		this.page = page;
	}
}

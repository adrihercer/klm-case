package com.afkl.cases.df.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Fare {

	private double amount = 0;
	private String currency = "";
	
	private ElementResponse from = new ElementResponse();
	private ElementResponse to = new ElementResponse();
	
	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public ElementResponse getFrom() {
		return from;
	}

	public void setFrom(ElementResponse from) {
		this.from = from;
	}

	public ElementResponse getTo() {
		return to;
	}

	public void setTo(ElementResponse to) {
		this.to = to;
	}
	
	public void copyFrom(Fare fare) {
		amount = fare.getAmount();
		currency = fare.getCurrency();
		from = fare.getFrom();
		to = fare.getTo();
	}
}

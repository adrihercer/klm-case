package com.afkl.cases.df.beans;

import java.util.Map;
import java.util.HashMap;

public class Stats {
	
	private long requestsProcessed = 0;
	private Map<Integer, Long> responsesByHttpCode = new HashMap<>();
	private long[] averageResponseData = new long[2];
	private int minResponseTime = Integer.MAX_VALUE;
	private int maxResponseTime = 0;
	
	public void registerNewRequest(Integer httpCode, Integer executionTime) {
		requestsProcessed++;
		
		averageResponseData[0] += executionTime;
		averageResponseData[1]++;
		
		if (executionTime < minResponseTime) {
			minResponseTime = executionTime;
		}
		
		if (executionTime > maxResponseTime) {
			maxResponseTime = executionTime;
		}
		
		Long httpCodeHits = responsesByHttpCode.getOrDefault(httpCode, 0L);
		responsesByHttpCode.put(httpCode, ++httpCodeHits);
	}

	public long getRequestsProcessed() {
		return requestsProcessed;
	}

	public Map<Integer, Long> getResponsesByHttpCode() {
		return responsesByHttpCode;
	}

	public long[] getAverageResponseData() {
		return averageResponseData;
	}

	public int getMinResponseTime() {
		return minResponseTime;
	}

	public int getMaxResponseTime() {
		return maxResponseTime;
	}
}

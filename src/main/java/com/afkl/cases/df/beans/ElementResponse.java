package com.afkl.cases.df.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ElementResponse {
	
	private String code = "";
	private String name = "";
	private String description = "";
	private ElementResponse parent;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public ElementResponse getParent() {
		return parent;
	}
	
	public void setParent(ElementResponse parent) {
		this.parent = parent;
	}
}

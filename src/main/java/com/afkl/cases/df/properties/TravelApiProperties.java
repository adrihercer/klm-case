package com.afkl.cases.df.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:travel-api.properties")
@ConfigurationProperties
public class TravelApiProperties {

	private String apiBaseUrl;
	private String authServicePath;
	private String airportsServicePath;
	private String faresServicePath;
	private String grantType;
	private String clientId;
	private String clientSecret;
	
	public String getApiBaseUrl() {
		return apiBaseUrl;
	}
	
	public void setApiBaseUrl(String apiBaseUrl) {
		this.apiBaseUrl = apiBaseUrl;
	}
	
	public String getAuthServicePath() {
		return authServicePath;
	}
	
	public void setAuthServicePath(String authServicePath) {
		this.authServicePath = authServicePath;
	}
	
	public String getAirportsServicePath() {
		return airportsServicePath;
	}
	
	public void setAirportsServicePath(String airportsServicePath) {
		this.airportsServicePath = airportsServicePath;
	}
	
	public String getFaresServicePath() {
		return faresServicePath;
	}
	
	public void setFaresServicePath(String faresServicePath) {
		this.faresServicePath = faresServicePath;
	}
	
	public String getGrantType() {
		return grantType;
	}

	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getAuthServiceUrl() {
		return apiBaseUrl + authServicePath + "?grant_type=" + grantType;
	}
	
	public String getAirportsServiceUrl() {
		return apiBaseUrl + airportsServicePath;
	}
	
	public String getFaresServiceUrl() {
		return apiBaseUrl + faresServicePath;
	}
}

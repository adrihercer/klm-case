package com.afkl.cases.df.services;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.afkl.cases.df.beans.AirportsList;
import com.afkl.cases.df.beans.ElementResponse;
import com.afkl.cases.df.properties.TravelApiProperties;

@Service
public class AirportsService extends BaseService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AirportsService.class);

	@Autowired
	public AirportsService(TravelApiProperties travelApiProperties, TravelApiTokenService travelApiTokenService) {
		super(travelApiProperties, travelApiTokenService);
	}
	
	@Async("KLM-Case-ThreadPoolTaskExecutor")
	public CompletableFuture<ElementResponse> getAirport(String code, int requestId) throws RestClientException {
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = setAuthorizationHeader(requestId);
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		
		LOGGER.info("[Request {}] Loading airport data for {}", requestId, code);
		
		ResponseEntity<ElementResponse> responseEntity = restTemplate.exchange(travelApiProperties.getAirportsServiceUrl() + "/" + code, HttpMethod.GET, entity, ElementResponse.class);
		
		return CompletableFuture.completedFuture(responseEntity.getBody());
	}
	
	public List<ElementResponse> findAirportByKeyword(String keyword, int requestId) throws RestClientException {
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = setAuthorizationHeader(requestId);
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		
		LOGGER.info("[Request {}] Searching airport by keyword {}", requestId, keyword);
		
		ResponseEntity<AirportsList> responseEntity = restTemplate.exchange(travelApiProperties.getAirportsServiceUrl() + "?term=" + keyword, HttpMethod.GET, entity, AirportsList.class);
		
		return responseEntity.getBody().getEmbedded().getLocations();
	}
	
	public List<ElementResponse> getAirports(int requestId, int listSize) throws RestClientException {
		
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = setAuthorizationHeader(requestId);
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		
		LOGGER.info("[Request {}] Loading airports list", requestId);
		
		String apiUrl = travelApiProperties.getAirportsServiceUrl() + (listSize > 1 ? "?size=" + listSize : "");
		
		ResponseEntity<AirportsList> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, AirportsList.class);
		
		return responseEntity.getBody().getEmbedded().getLocations();
	}
	
	public List<ElementResponse> getAllAirports(int requestId) throws RestClientException {
		
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = setAuthorizationHeader(requestId);
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		
		LOGGER.info("[Request {}] Loading airports list to get full data size", requestId);
		
		ResponseEntity<AirportsList> responseEntity = restTemplate.exchange(travelApiProperties.getAirportsServiceUrl(), HttpMethod.GET, entity, AirportsList.class);
		
		int totalAirports = responseEntity.getBody().getPage().getTotalElements();
		
		LOGGER.info("[Request {}] Loading all airports list: {}", requestId, totalAirports);
		
		return getAirports(requestId, totalAirports);
	}
}
package com.afkl.cases.df.services;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.afkl.cases.df.beans.Stats;

@Service
public class StatsService {
	
	private Stats stats = new Stats();
	
	@Async
	public void registerRequest(Integer httpCode, Integer executionTime) {
		synchronized(this) {
			stats.registerNewRequest(httpCode, executionTime);
		}
	}
	
	public Stats getStats() {
		return stats;
	}

}

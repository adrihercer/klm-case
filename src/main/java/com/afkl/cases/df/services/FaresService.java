package com.afkl.cases.df.services;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.afkl.cases.df.beans.Fare;
import com.afkl.cases.df.properties.TravelApiProperties;

@Service
public class FaresService extends BaseService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FaresService.class);

	@Autowired
	public FaresService(TravelApiProperties travelApiProperties, TravelApiTokenService travelApiTokenService) {
		super(travelApiProperties, travelApiTokenService);
	}
	
	@Async("KLM-Case-ThreadPoolTaskExecutor")
	public CompletableFuture<Fare> getFares(String codeFrom, String codeTo, int requestId) throws RestClientException {
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = setAuthorizationHeader(requestId);
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		
		LOGGER.info("[Request {}] Loading fare data from {} to {}", requestId, codeFrom, codeTo);
		
		ResponseEntity<Fare> responseEntity = restTemplate.exchange(travelApiProperties.getFaresServiceUrl() + "/" + codeFrom + "/" + codeTo, HttpMethod.GET, entity, Fare.class);
		
		return CompletableFuture.completedFuture(responseEntity.getBody());
	}
}

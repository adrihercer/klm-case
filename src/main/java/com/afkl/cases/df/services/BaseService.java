package com.afkl.cases.df.services;

import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestClientException;

import com.afkl.cases.df.properties.TravelApiProperties;

public abstract class BaseService {
	
	protected TravelApiProperties travelApiProperties;
	protected TravelApiTokenService travelApiTokenService;
	protected StatsService statsService;
	
	protected BaseService(TravelApiProperties travelApiProperties, TravelApiTokenService travelApiTokenService) {
		this.travelApiProperties = travelApiProperties;
		this.travelApiTokenService = travelApiTokenService;
	}
	
	protected HttpHeaders setAuthorizationHeader(int requestId) throws RestClientException {
	   return new HttpHeaders() {{
		   String authHeader = "Bearer " + travelApiTokenService.getTravelApiToken(requestId);
	       set( "Authorization", authHeader );
	   }};
	}
}

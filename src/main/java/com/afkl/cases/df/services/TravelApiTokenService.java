package com.afkl.cases.df.services;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.afkl.cases.df.beans.TokenResponse;
import com.afkl.cases.df.properties.TravelApiProperties;

@Service
public class TravelApiTokenService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TravelApiTokenService.class);
	
	private TravelApiProperties travelApiProperties;
	
	@Autowired
	public TravelApiTokenService(TravelApiProperties travelApiProperties) {
		this.travelApiProperties = travelApiProperties;
	}
	
	public String getTravelApiToken(int requestId) throws RestClientException {
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = createHeaders(travelApiProperties.getClientId(), travelApiProperties.getClientSecret());
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		
		LOGGER.info("[Request {}] Requesting token to consume API", requestId);
		
		TokenResponse tokenResponse = restTemplate.postForObject(travelApiProperties.getAuthServiceUrl(), entity, TokenResponse.class);
		
		return tokenResponse.getAccessToken();
	}
	
	private HttpHeaders createHeaders(String username, String password){
		return new HttpHeaders() {{
	         String auth = username + ":" + password;
	         byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
	         String authHeader = "Basic " + new String(encodedAuth);
	         set( "Authorization", authHeader );
	      }};
	}

}

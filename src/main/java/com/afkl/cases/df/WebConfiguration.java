package com.afkl.cases.df;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@PropertySource("classpath:runtime.properties")
@EnableWebMvc
public class WebConfiguration extends WebMvcConfigurerAdapter {
	
	@Value("${mode}")
    private String runtimeMode;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
    	if ("development".equalsIgnoreCase(runtimeMode)) {
    		registry.addMapping("/airports/**").allowedOrigins("http://localhost:8081");
    		registry.addMapping("/fares/**").allowedOrigins("http://localhost:8081");
    		registry.addMapping("/stats").allowedOrigins("http://localhost:8081");
    	}
    }
}

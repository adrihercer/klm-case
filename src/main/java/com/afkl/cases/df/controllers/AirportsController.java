package com.afkl.cases.df.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.beans.ElementResponse;
import com.afkl.cases.df.services.AirportsService;
import com.afkl.cases.df.services.StatsService;

@RestController
@RequestMapping("/airports")
public class AirportsController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AirportsController.class);

	private AirportsService airportService;
	
	@Autowired
	public AirportsController(AirportsService airportService, StatsService statsService) {
		super(statsService);
		this.airportService = airportService;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<ElementResponse> list() {
		long start = System.currentTimeMillis();
		int requestId = Calendar.getInstance().hashCode();
		int statusCode = HttpStatus.OK_200;
		List<ElementResponse> airports = new ArrayList<>();
		
		try {
			airports = airportService.getAirports(requestId, -1);
			
			if (airports.isEmpty()) {
				statusCode = HttpStatus.NOT_FOUND_404;
			}
		} catch (Exception ex) {
			statusCode = HttpStatus.INTERNAL_SERVER_ERROR_500;
			LOGGER.error("[Request {}] Error loading the airports list", requestId, ex);
		} finally {
			long end = System.currentTimeMillis();
			statsService.registerRequest(statusCode, ((int) end) - ((int) start));
		}
		
		return airports;
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<ElementResponse> listAll() {
		long start = System.currentTimeMillis();
		int requestId = Calendar.getInstance().hashCode();
		int statusCode = HttpStatus.OK_200;
		List<ElementResponse> airports = new ArrayList<>();
		
		try {
			airports = airportService.getAllAirports(requestId);
			
			if (airports.isEmpty()) {
				statusCode = HttpStatus.NOT_FOUND_404;
			}
		} catch (Exception ex) {
			statusCode = HttpStatus.INTERNAL_SERVER_ERROR_500;
			LOGGER.error("[Request {}] Error loading the airports list", requestId, ex);
		} finally {
			long end = System.currentTimeMillis();
			statsService.registerRequest(statusCode, ((int) end) - ((int) start));
		}
		
		return airports;
	}
	
	@RequestMapping(value = "/find", params = "keyword", method = RequestMethod.GET)
	public List<ElementResponse> find(@RequestParam("keyword") String keyword) {
		long start = System.currentTimeMillis();
		int requestId = Calendar.getInstance().hashCode();
		int statusCode = HttpStatus.OK_200;
		List<ElementResponse> airports = new ArrayList<>();
		
		try {
			airports = airportService.findAirportByKeyword(keyword, requestId);
			
			if (airports.isEmpty()) {
				statusCode = HttpStatus.NOT_FOUND_404;
			}
		} catch (Exception ex) {
			statusCode = HttpStatus.INTERNAL_SERVER_ERROR_500;
			LOGGER.error("[Request {}] Error searching airports for keyword '{}'", requestId, keyword, ex);
		} finally {
			long end = System.currentTimeMillis();
			statsService.registerRequest(statusCode, ((int) end) - ((int) start));
		}
		
		return airports;
	}
	
	@RequestMapping(value = "/get/{code}", method = RequestMethod.GET)
	public ElementResponse get(@PathVariable("code") String code) {
		long start = System.currentTimeMillis();
		int requestId = Calendar.getInstance().hashCode();
		int statusCode = HttpStatus.OK_200;
		ElementResponse response = new ElementResponse();
		
		try {
			CompletableFuture<ElementResponse> airportData = airportService.getAirport(code, requestId);
			CompletableFuture.allOf(airportData).join();
			
			response = airportData.get();
			
			if (response.getCode().isEmpty() && response.getDescription().isEmpty()) {
				statusCode = HttpStatus.NOT_FOUND_404;
			}
		} catch (Exception ex) {
			statusCode = HttpStatus.INTERNAL_SERVER_ERROR_500;
			LOGGER.error("[Request {}] Error loading airport data for {}", requestId, code, ex);
		} finally {
			long end = System.currentTimeMillis();
			statsService.registerRequest(statusCode, ((int) end) - ((int) start));
		}
		
		return response;
	}
	
}

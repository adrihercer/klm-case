package com.afkl.cases.df.controllers;

import com.afkl.cases.df.services.StatsService;

public abstract class BaseController {

	protected StatsService statsService;
	
	public BaseController(StatsService statsService) {
		this.statsService = statsService;
	}
}

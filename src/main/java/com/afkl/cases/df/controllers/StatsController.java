package com.afkl.cases.df.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.beans.Stats;
import com.afkl.cases.df.services.StatsService;

@RestController
@RequestMapping("/stats")
public class StatsController {
	
	private StatsService statsService;
	
	@Autowired
	public StatsController(StatsService statsService) {
		this.statsService = statsService;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public Stats get() {
		return statsService.getStats();
	}

}

package com.afkl.cases.df.controllers;

import java.util.Calendar;
import java.util.concurrent.CompletableFuture;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.beans.ElementResponse;
import com.afkl.cases.df.beans.Fare;
import com.afkl.cases.df.services.AirportsService;
import com.afkl.cases.df.services.FaresService;
import com.afkl.cases.df.services.StatsService;

@RestController
@RequestMapping("/fares/{from}/{to}")
public class FaresController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FaresController.class);
	
	private FaresService faresService;
	private AirportsService airportsService;

	@Autowired
	public FaresController(FaresService faresService, AirportsService airportsService, StatsService statsService) {
		super(statsService);
		this.faresService = faresService;
		this.airportsService = airportsService;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public Fare get(@PathVariable("from") String from, @PathVariable("to") String to) {
		long start = System.currentTimeMillis();
		int requestId = Calendar.getInstance().hashCode();
		Fare fareResponse = new Fare();
		int statusCode = HttpStatus.OK_200;
		
		try {
			
			CompletableFuture<Fare> fareData = faresService.getFares(from, to, requestId);
			CompletableFuture<ElementResponse> fromAirportData = airportsService.getAirport(from, requestId);
			CompletableFuture<ElementResponse> toAirportData = airportsService.getAirport(to, requestId);
			
			CompletableFuture.allOf(fareData, fromAirportData, toAirportData).join();
			
			
			fareResponse.copyFrom(fareData.get());
			fareResponse.setFrom(fromAirportData.get());
			fareResponse.setTo(toAirportData.get());
			
			if (fareResponse.getAmount() == 0 && fareResponse.getCurrency().isEmpty()) {
				statusCode = HttpStatus.NOT_FOUND_404;
			}
		} catch (Exception ex) {
			statusCode = HttpStatus.INTERNAL_SERVER_ERROR_500;
			LOGGER.error("[Request {}] Error loading fare from {} to {}", requestId, from, to, ex);
		} finally {
			long end = System.currentTimeMillis();
			statsService.registerRequest(statusCode, ((int) end) - ((int) start));
		}
		
		return fareResponse;
	}
}

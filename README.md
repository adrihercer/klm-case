Travel API Client 
=================

This project is made of two pieces:

1. This projects that contains the backend.
2. The project in [https://bitbucket.org/adrihercer/klm-case-frontend](https://bitbucket.org/adrihercer/klm-case-frontend) that contains the Angular code for the frontend.

In order to compile and executed the project successfully, please follow the next steps:

1. Clone this repo 
2. In the same directory where you have close this project clone the klm-case-frontend project (Note: now the klm-case and klm-case-frontend should be siblings in the same directory).
3. Start the klm-case project (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the app (after starting the application) go to:

[http://localhost:9000/travel/pages/index.html](http://localhost:9000/travel/pages/index.html)

The accompished items on this project are:
###### Create a user interface
- [X] Be able to select an origin.
- [X] Be able to select a destination.
- [X] Retrieve a fare offer for the given origin-destination combination.
- [X] Selections for origin and/or destination should be searchable.
###### Create a backend supporting the user interface
- [X] The user interface requires a dedicated backend that supports it, build this in the provided template using spring-boot.
- [X] Consume the mocked services from the service mock project.
- [X] The authentication is on application level, so make sure the user interface is not bothered with authentication.
- [X] Implement an efficient way to perform service calls in parallel.
###### Add statistics for your backend
- [X] Total number of requests processed
- [X] Total number of requests resulted in an OK response
- [X] Total number of requests resulted in a 4xx response
- [X] Total number of requests resulted in a 5xx response
- [X] Average response time of all requests
- [X] Min response time of all requests
- [X] Max response time of all requests
- [X] Expose this information in a new restful endpoint as JSON and create a new dashboard to visualize this info.
- [X] Collecting metrics should not impact the user experience in any way.
###### Make the application configurable
- [X] No hard coded values for things like endpoints, etc. Everything should be configurable in some kind of configuration file.
###### Extra assignment fullstack developer
- [X] Create a separate build for your frontend (using npm and webpack) and build your frontend in angular (which version is up to your own preference) and make sure that somehow your resulting artifact is included and exposed in your boot project.
###### Bonus points!
- [X] Create a new page where you can see a list of all airports (and be able to search on them) and paginate the results.
- [X] The mock service does not provide any form or sorting, find a way to add sorting without having to change the original mock
- [X] Provide a thread name that looks like 'async-task-{thread_id}' for all threads that perform async tasks.
- [X] Provide a unique ID to each request for tracking purposes and have <strong>every</strong> log line include it before the message.
- [X] Show us your knowledge of java up to and including the latest released version!